# pushrod-widgets

[![Build Status](https://travis-ci.org/KenSuenobu/pushrod-widgets.svg?branch=master)](https://travis-ci.org/KenSuenobu/pushrod-widgets)
[![](https://img.shields.io/crates/d/pushrod-widgets.svg)](https://crates.io/crates/pushrod-widgets)
[![docs.rs for pushrod-events](https://docs.rs/pushrod-widgets/badge.svg)](https://docs.rs/pushrod-widgets)
[![Gitter Chat](https://badges.gitter.im/rust-pushrod/community.svg)](https://gitter.im/rust-pushrod/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

Pushrod Widgets is the widget support library for the Pushrod project.  It contains custom events,
properties, default widgets, and a cache.

This library is part of the Pushrod library, which contains the following:

- [Pushrod](https://www.github.com/KenSuenobu/pushrod/) main library, containing the controller
- [Pushrod-Widgets](https://www.github.com/KenSuenobu/pushrod-widgets/) widgets library, containing the master widgets
