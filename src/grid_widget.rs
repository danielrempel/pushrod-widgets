// Pushrod Widgets
// Grid Widget
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use sdl2::render::{Canvas, Texture};
use sdl2::video::Window;

use pushrod::caches::TextureCache;
use pushrod::primitives::draw_base;
use pushrod::properties::{
    WidgetProperties, PROPERTY_GRID_COLOR, PROPERTY_GRID_CONNECTED, PROPERTY_GRID_SPACING,
};
use pushrod::texture_store::TextureStore;
use pushrod::widget::Widget;
use pushrod::widget_default_impl;

use sdl2::pixels::Color;
use sdl2::rect::Point;

/// Base Widget.
#[derive(Default)]
pub struct GridWidget {
    texture_store: TextureStore,
    properties: WidgetProperties,
}

/// Implementation for drawing a `BaseWidget`, with the `Widget` trait objects applied.
impl Widget for GridWidget {
    widget_default_impl!("GridWidget");

    /// This is a `GridWidget` that is used as a standard blank `Widget`.
    ///
    /// - PROPERTY_MAIN_COLOR: the `Color` of the body of the `Widget`
    /// - PROPERTY_BORDER_WIDTH: the width of the border to draw
    /// - PROPERTY_BORDER_COLOR: the `Color` of the border
    /// - PROPERTY_GRID_COLOR: the `Color` of the grid if set, otherwise, `Color::GRAY` is used
    /// - PROPERTY_GRID_SPACING: the number of pixels between each grid point/line
    /// - PROPERTY_GRID_CONNECTED: boolean indicating whether or not the grid contains lines (true) or points (false)
    fn draw(&mut self, c: &mut Canvas<Window>, _t: &mut TextureCache) -> Option<&Texture> {
        // ONLY update the texture if the `BaseWidget` shows that it's been invalidated.
        if self.invalidated() {
            let bounds = self.properties.get_bounds();
            let grid_connections = self.properties.get_bool(PROPERTY_GRID_CONNECTED);
            let grid_size = self.properties.get_value(PROPERTY_GRID_SPACING) as usize;
            let grid_draw_color = self.properties.get_color(PROPERTY_GRID_COLOR, Color::GRAY);

            self.texture_store
                .create_or_resize_texture(c, bounds.0, bounds.1);

            let cloned_properties = self.properties.clone();

            c.with_texture_canvas(self.texture_store.get_mut_ref(), |texture| {
                draw_base(texture, &cloned_properties, None);

                texture.set_draw_color(grid_draw_color);

                if grid_connections {
                    for i in (0..bounds.0).step_by(grid_size) {
                        texture
                            .draw_line(
                                Point::new(i as i32, 0),
                                Point::new(i as i32, bounds.1 as i32),
                            )
                            .unwrap();
                    }

                    for i in (0..bounds.1).step_by(grid_size) {
                        texture
                            .draw_line(
                                Point::new(0, i as i32),
                                Point::new(bounds.0 as i32, i as i32),
                            )
                            .unwrap();
                    }
                } else {
                    for x in (0..bounds.0).step_by(grid_size) {
                        for y in (0..bounds.1).step_by(grid_size) {
                            texture.draw_point(Point::new(x as i32, y as i32)).unwrap();
                        }
                    }
                }
            })
            .unwrap();
        }

        self.texture_store.get_optional_ref()
    }
}
