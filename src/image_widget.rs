// Pushrod Widgets
// Image Widget
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use pushrod::caches::TextureCache;
use pushrod::primitives::draw_base;
use pushrod::properties::{
    WidgetProperties, PROPERTY_BORDER_WIDTH, PROPERTY_IMAGE_FILENAME, PROPERTY_IMAGE_POSITION,
    PROPERTY_IMAGE_SCALED,
};
use pushrod::texture_store::TextureStore;
use pushrod::widget::Widget;
use pushrod::widget_default_impl;

use sdl2::rect::Rect;
use sdl2::render::{Canvas, Texture, TextureQuery};
use sdl2::video::Window;

/// Image position is upper left-hand corner
pub const COMPASS_NW: i32 = 1;

/// Image position is top centered
pub const COMPASS_N: i32 = 2;

/// Image position is upper right-hand corner
pub const COMPASS_NE: i32 = 3;

/// Image position is centered on the right-most bounds
pub const COMPASS_E: i32 = 4;

/// Image position is lower right-hand corner
pub const COMPASS_SE: i32 = 5;

/// Image position is bottom centered
pub const COMPASS_S: i32 = 6;

/// Image position is lower left-hand corner
pub const COMPASS_SW: i32 = 7;

/// Image position is centered on the left-most bounds
pub const COMPASS_W: i32 = 8;

/// Image position is in the center of the `ImageWidget`
pub const COMPASS_CENTER: i32 = 9;

/// This is the storage object for the `ImageWidget`.  It stores the config, properties, callback registry,
/// the image name, and a scale flag.
#[derive(Default)]
pub struct ImageWidget {
    texture_store: TextureStore,
    properties: WidgetProperties,
}

/// Creates a new `ImageWidget`, which draws an image in a supported image format for SDL2 at a specific
/// location on the screen.  Requires the name of the image (the full path to the file), the position
/// within the widget (defined as `ImagePosition`), the xywh bounds, and whether or not the image is
/// scaled within the bounds of the `Widget`.
impl Widget for ImageWidget {
    widget_default_impl!("ImageWidget");

    /// This is the drawing implementation of the `ImageWidget`.  The following are the properties that can
    /// be applied:
    ///
    /// - PROPERTY_MAIN_COLOR: the `Color` of the body of the `Widget`
    /// - PROPERTY_BORDER_WIDTH: the width of the border to draw
    /// - PROPERTY_BORDER_COLOR: the `Color` of the border.
    /// - PROPERTY_IMAGE_POSITION: position of the image within the `ImageWidget`
    /// - PROPERTY_IMAGE_FILENAME: the filename of the image to display
    /// - PROPERTY_IMAGE_SCALED: boolean flag indicating whether or not to scale the image to the bounds of the `ImageWidget`
    fn draw(&mut self, c: &mut Canvas<Window>, t: &mut TextureCache) -> Option<&Texture> {
        if self.invalidated() {
            let bounds = self.properties.get_bounds();

            self.texture_store
                .create_or_resize_texture(c, bounds.0, bounds.1);

            let widget_w = bounds.0;
            let widget_h = bounds.1;
            let image_texture = t.get_image(c, self.properties.get(PROPERTY_IMAGE_FILENAME));
            let TextureQuery { width, height, .. } = image_texture.query();
            let scaled = self.properties.get_bool(PROPERTY_IMAGE_SCALED);

            let texture_x = match self.properties.get_value(PROPERTY_IMAGE_POSITION) {
                COMPASS_NW | COMPASS_W | COMPASS_SW => 0,

                COMPASS_N | COMPASS_CENTER | COMPASS_S => (widget_w - width) / 2,

                COMPASS_NE | COMPASS_E | COMPASS_SE => widget_w - width,

                _ => 0,
            };

            let texture_y = match self.properties.get_value(PROPERTY_IMAGE_POSITION) {
                COMPASS_NW | COMPASS_N | COMPASS_NE => 0,

                COMPASS_W | COMPASS_CENTER | COMPASS_E => (widget_h - height) / 2,

                COMPASS_SW | COMPASS_S | COMPASS_SE => widget_h - height,

                _ => 0,
            };

            let cloned_properties = self.properties.clone();

            c.with_texture_canvas(self.texture_store.get_mut_ref(), |texture| {
                draw_base(texture, &cloned_properties, None);

                let border_width = cloned_properties.get_value(PROPERTY_BORDER_WIDTH);

                if !scaled {
                    texture
                        .copy(
                            image_texture,
                            None,
                            Rect::new(
                                texture_x as i32 + border_width,
                                texture_y as i32 + border_width,
                                width - (border_width as u32 * 2),
                                height - (border_width as u32 * 2),
                            ),
                        )
                        .unwrap();
                } else {
                    texture
                        .copy(
                            image_texture,
                            None,
                            Rect::new(
                                border_width,
                                border_width,
                                widget_w as u32 - (border_width as u32 * 2),
                                widget_h as u32 - (border_width as u32 * 2),
                            ),
                        )
                        .unwrap();
                }
            })
            .unwrap();
        }

        self.texture_store.get_optional_ref()
    }
}
