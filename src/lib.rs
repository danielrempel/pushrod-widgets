// Pushrod Widgets
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// This is a text `Widget` that draws text given a style, justification, font name, size, and
/// body of text.
pub mod text_widget;

/// This is an image `Widget` that draws an image (in any of the supported image formats that SDL2 provides).
pub mod image_widget;

/// This is a `Widget` that can be used to show a progress bar.
pub mod progress_widget;

/// This is a `Widget` that provides button functionality.
pub mod button_widget;

/// This is a `Widget` that draws a grid inside the body of the `Widget`.
pub mod grid_widget;

/// This is a `Widget` that draws a grouping box.
pub mod group_box_widget;

/// This is a `Widget` that can be selected/toggled on or off.
pub mod toggle_button_widget;

/// This is a `Widget` that acts as a `ButtonWidget`, but with an image.
pub mod image_button_widget;

/// This is a `Widget` that shows a checkbox, indicating an `"X"` inside the box when selected,
/// and a blank box otherwise.
pub mod checkbox_widget;

/// This is a `Widget` that allows for a selection of one object in a group.
pub mod radio_button_widget;

/// This is a `Widget` that, when hovered, will be highlighted, and the highlight will disappear
/// once the button is clicked.
pub mod menu_item_widget;

/// This is a `Widget` that displays a popup menu of items that can be selected, and once selected,
/// the parent object will be hidden from view.
pub mod popup_menu_widget;
