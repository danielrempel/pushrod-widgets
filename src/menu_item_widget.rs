// Pushrod Widgets
// Menu Item Widget
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use sdl2::render::{Canvas, Texture};
use sdl2::video::Window;

use pushrod::caches::TextureCache;
use pushrod::event::PushrodEvent;
use pushrod::event::PushrodEvent::{
    DrawFrame, MouseButton, WidgetMenuItemSelected, WidgetMouseEntered, WidgetMouseExited,
};
use pushrod::primitives::draw_base;
use pushrod::properties::{
    WidgetProperties, PROPERTY_BORDER_WIDTH, PROPERTY_DISABLED, PROPERTY_ID, PROPERTY_MENU_ITEM_ID,
};
use pushrod::texture_store::TextureStore;
use pushrod::widget::Widget;
use pushrod::widget_default_impl;

use sdl2::pixels::Color;
use sdl2::rect::Rect;

/// Internal flag indicating whether or not this `Widget` has the mouse currently hovered over it, and
/// the button state is highlighted.
const PROPERTY_BUTTON_HOVERED: u32 = 20003;

/// Base Widget.
#[derive(Default)]
pub struct MenuItemWidget {
    texture_store: TextureStore,
    properties: WidgetProperties,
}

/// Auxiliary functions for the `MenuItemWidget`.
impl MenuItemWidget {
    /// Sets the state to hovered, meaning the mouse has entered the bounds of the `Widget`.
    fn set_hovered(&mut self) {
        self.properties.set_bool(PROPERTY_BUTTON_HOVERED);
        self.invalidate();
    }

    /// Sets the state to unhovered, meaning the mouse has left the bounds of the `Widget`.
    fn set_unhovered(&mut self) {
        self.properties.delete(PROPERTY_BUTTON_HOVERED);
        self.invalidate();
    }

    fn is_disabled(&self) -> bool {
        self.properties.get_bool(PROPERTY_DISABLED)
    }
}

/// Implementation for drawing a `MenuItemWidget`, with the `Widget` trait objects applied.
impl Widget for MenuItemWidget {
    widget_default_impl!("MenuItemWidget");

    /// This is a `ButtonWidget` that is used as a standard blank `Widget`.
    ///
    /// - PROPERTY_MAIN_COLOR: the `Color` of the body of the `Widget`
    /// - PROPERTY_FONT_COLOR: the color of the font
    /// - PROPERTY_FONT_NAME: full or relative path to the font file to use to render the text
    /// - PROPERTY_FONT_SIZE: the size in points of the font
    /// - PROPERTY_FONT_STYLE: the `FontStyle` to apply to the font
    /// - PROPERTY_TEXT: `String` containing the text to display
    /// - PROPERTY_DISABLED: sets the object disabled, displaying the text, but with an alpha of 128.
    ///
    /// While the button is selected, the color of the background will be `Color::BLACK`, and the
    /// text will turn `Color::WHITE`.  If the button is _not_ selected, it will revert back to the
    /// colors that you set for the text color and background colors of the button.
    fn draw(&mut self, c: &mut Canvas<Window>, t: &mut TextureCache) -> Option<&Texture> {
        // ONLY update the texture if the `BaseWidget` shows that it's been invalidated.
        if self.invalidated() {
            let bounds = self.properties.get_bounds();
            let text_color =
                if self.properties.get_bool(PROPERTY_BUTTON_HOVERED) && !self.is_disabled() {
                    Some(Color::RGB(255, 255, 255))
                } else if self.is_disabled() {
                    Some(Color::RGBA(0, 0, 0, 128))
                } else {
                    None
                };
            let (font_texture, width, height) = t.render_text(c, &mut self.properties, text_color);
            let border_width = self.properties.get_value(PROPERTY_BORDER_WIDTH);
            let texture_x: i32 = 20;

            self.texture_store
                .create_or_resize_texture(c, bounds.0, bounds.1);

            let mut cloned_properties = self.properties.clone();
            let back_color =
                if self.properties.get_bool(PROPERTY_BUTTON_HOVERED) && !self.is_disabled() {
                    Some(Color::RGB(0, 0, 0))
                } else {
                    None
                };

            cloned_properties.set_value(PROPERTY_BORDER_WIDTH, 0);

            c.with_texture_canvas(self.texture_store.get_mut_ref(), |texture| {
                draw_base(texture, &cloned_properties, back_color);

                texture
                    .copy(
                        &font_texture,
                        None,
                        Rect::new(
                            texture_x + border_width,
                            ((bounds.1 / 2) - (height / 2) - (border_width / 2) as u32) as i32,
                            width,
                            height,
                        ),
                    )
                    .unwrap();
            })
            .unwrap();
        }

        self.texture_store.get_optional_ref()
    }

    /// Overrides the `handle_event` function, which handles the mouse button, widget entering and
    /// exiting events.
    fn handle_event(&mut self, event: PushrodEvent) -> Option<PushrodEvent> {
        match event {
            WidgetMouseEntered { .. } => self.set_hovered(),
            WidgetMouseExited { .. } => self.set_unhovered(),
            MouseButton {
                widget_id,
                button,
                state,
            } => {
                let current_widget_id = self.properties.get_value(PROPERTY_ID) as u32;

                if widget_id == current_widget_id && button == 1 && !state && !self.is_disabled() {
                    let menu_item = self.properties.get_value(PROPERTY_MENU_ITEM_ID) as u32;

                    return Some(WidgetMenuItemSelected {
                        widget_id,
                        menu_item,
                    });
                }
            }
            DrawFrame { .. } => {}
            _ => eprintln!("ButtonWidget Event: {:?}", event),
        }

        None
    }
}
