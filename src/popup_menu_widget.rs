// Pushrod Widgets
// Popup Menu Widget
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use sdl2::render::{Canvas, Texture};
use sdl2::video::Window;
use sdl2::pixels::Color;

use pushrod::caches::TextureCache;
use pushrod::primitives::draw_base;
use pushrod::properties::{WidgetProperties, PROPERTY_BORDER_COLOR, PROPERTY_BORDER_WIDTH, PROPERTY_FONT_NAME, PROPERTY_FONT_SIZE, PROPERTY_FONT_STYLE, PROPERTY_MAIN_COLOR, PROPERTY_MENU_ITEM_ID, PROPERTY_TEXT, PROPERTY_HIDDEN};
use pushrod::base_widget::BaseWidget;
use pushrod::texture_store::TextureStore;
use pushrod::widget::Widget;
use pushrod::widget_default_impl;

use crate::menu_item_widget::MenuItemWidget;

/// Base Widget.
#[derive(Default)]
pub struct PopupMenuWidget {
    texture_store: TextureStore,
    properties: WidgetProperties,
    menu_child_ids: Vec<u32>,
    layout_built: bool,
}

/// Implementation for drawing a `PopupMenuWidget`, with the `Widget` trait objects applied.
impl Widget for PopupMenuWidget {
    widget_default_impl!("PopupMenuWidget");

    /// This is a `PopupMenuWidget` that is used as a standard blank `Widget`.
    ///
    /// - PROPERTY_MAIN_COLOR: the `Color` of the body of the `Widget`
    /// - PROPERTY_BORDER_WIDTH: the width of the border to draw
    /// - PROPERTY_BORDER_COLOR: the `Color` of the border.
    fn draw(&mut self, c: &mut Canvas<Window>, _t: &mut TextureCache) -> Option<&Texture> {
        // ONLY update the texture if the `BaseWidget` shows that it's been invalidated.
        if self.invalidated() {
            let bounds = self.properties.get_bounds();

            self.texture_store
                .create_or_resize_texture(c, bounds.0, bounds.1);

            let cloned_properties = self.properties.clone();

            c.with_texture_canvas(self.texture_store.get_mut_ref(), |texture| {
                draw_base(texture, &cloned_properties, None)
            })
            .unwrap();
        }

        self.texture_store.get_optional_ref()
    }

    /// This function builds the menu of items that will be displayed inside the `PopupMenuWidget`.
    /// They include single element `PopupMenuItemWidget` objects based on their IDs.  Once an ID
    /// is clicked, the engine generates a `WidgetMenuItemSelected` event.
    fn build_layout(&mut self) -> Vec<Box<dyn Widget>> {
        if !self.layout_built {
            let mut return_vec: Vec<Box<dyn Widget>> = vec![];
            let origin = self.properties.get_origin();
            let origin_x = origin.0 + 1;
            let mut origin_y = origin.1 + 1;
            let mut menu_item = 1;

            for line in self
                .properties()
                .get(PROPERTY_TEXT)
                .lines()
                .collect::<Vec<&str>>()
            {
                let mut menu_item_widget = MenuItemWidget::default();

                menu_item_widget
                    .properties()
                    .set_origin(origin_x, origin_y)
                    .set_bounds(140, 20)
                    .set_color(PROPERTY_MAIN_COLOR, Color::WHITE)
                    .set(
                        PROPERTY_FONT_NAME,
                        String::from("assets/OpenSans-Regular.ttf"),
                    )
                    .set_value(PROPERTY_FONT_SIZE, 14)
                    .set_value(PROPERTY_FONT_STYLE, sdl2::ttf::FontStyle::NORMAL.bits())
                    .set_value(PROPERTY_MENU_ITEM_ID, menu_item)
                    .set_bool(PROPERTY_HIDDEN)
                    .set(PROPERTY_TEXT, String::from(line));

                return_vec.push(Box::new(menu_item_widget));

                origin_y += 20;
                menu_item += 1;
            }

            let mut base_widget = BaseWidget::default();

            base_widget
                .properties()
                .set_origin(origin.0, origin.1)
                .set_bounds(142, (menu_item * 20) as u32 - 18)
                .set_value(PROPERTY_BORDER_WIDTH, 1)
                .set_color(PROPERTY_BORDER_COLOR, Color::BLACK)
                .set_color(PROPERTY_MAIN_COLOR, Color::WHITE)
                .set_bool(PROPERTY_HIDDEN);

            return_vec.insert(0, Box::new(base_widget));

            eprintln!("Building layout: {} items", return_vec.len());

            self.layout_built = true;

            return_vec
        } else {
            vec![]
        }
    }

    /// After a `build_layout` call has been completed, the list of IDs that were added to the display
    /// list are returned in the same order in from `build_layout`.  The `Widget` that created
    /// these IDs should then store them here in an array, so that they can be referenced when
    /// interacted with.
    fn constructed_layout_ids(&mut self, ids: Vec<u32>) {
        self.menu_child_ids = ids;
    }
}
