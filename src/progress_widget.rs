// Pushrod Widgets
// Progress Widget
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use sdl2::render::{Canvas, Texture};
use sdl2::video::Window;

use pushrod::caches::TextureCache;
use pushrod::primitives::{draw_base, fill_box};
use pushrod::properties::{
    WidgetProperties, PROPERTY_BORDER_WIDTH, PROPERTY_PROGRESS, PROPERTY_PROGRESS_COLOR,
};
use pushrod::texture_store::TextureStore;
use pushrod::widget::Widget;
use pushrod::widget_default_impl;

use sdl2::pixels::Color;

/// Base Widget.
#[derive(Default)]
pub struct ProgressWidget {
    texture_store: TextureStore,
    properties: WidgetProperties,
}

/// Implementation for drawing a `ProgressWidget`, with the `Widget` trait objects applied.
impl Widget for ProgressWidget {
    widget_default_impl!("ProgressWidget");

    /// Draws the `ProgressWidget` using the following properties:
    ///
    /// - PROPERTY_MAIN_COLOR: the `Color` of the body of the `Widget`
    /// - PROPERTY_BORDER_WIDTH: the width of the border to draw
    /// - PROPERTY_BORDER_COLOR: the `Color` of the border.
    /// - PROPERTY_PROGRESS_COLOR: the `Color` of the progress bar
    /// - PROPERTY_PROGRESS: the progress value (from 0 to 100)
    fn draw(&mut self, c: &mut Canvas<Window>, _t: &mut TextureCache) -> Option<&Texture> {
        // ONLY update the texture if the `BaseWidget` shows that it's been invalidated.
        if self.invalidated() {
            let bounds = self.properties.get_bounds();
            let border_width = self.properties.get_value(PROPERTY_BORDER_WIDTH);

            self.texture_store
                .create_or_resize_texture(c, bounds.0, bounds.1);

            let cloned_properties = self.properties.clone();

            c.with_texture_canvas(self.texture_store.get_mut_ref(), |texture| {
                draw_base(texture, &cloned_properties, None);

                let inside_color = cloned_properties.get_color(PROPERTY_PROGRESS_COLOR, Color::RED);
                let progress = cloned_properties.get_value(PROPERTY_PROGRESS);
                let start_x = border_width;
                let start_y = border_width;
                let progress_width = (f64::from(bounds.0) * (f64::from(progress) / 100.0)) as u32
                    - (border_width * 2) as u32;
                let progress_height = bounds.1 - (border_width * 2) as u32;

                fill_box(
                    texture,
                    start_x as u32,
                    start_y as u32,
                    progress_width,
                    progress_height,
                    inside_color,
                );
            })
            .unwrap();
        }

        self.texture_store.get_optional_ref()
    }
}
