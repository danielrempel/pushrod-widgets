// Pushrod Widgets
// Text Widget
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use sdl2::render::{Canvas, Texture};
use sdl2::video::Window;

use pushrod::caches::TextureCache;
use pushrod::primitives::draw_base;
use pushrod::properties::{
    WidgetProperties, PROPERTY_TEXT_JUSTIFICATION, TEXT_JUSTIFY_CENTER, TEXT_JUSTIFY_LEFT,
    TEXT_JUSTIFY_RIGHT,
};
use pushrod::texture_store::TextureStore;
use pushrod::widget::Widget;
use pushrod::widget_default_impl;

use sdl2::rect::Rect;

/// Text Widget.
#[derive(Default)]
pub struct TextWidget {
    texture_store: TextureStore,
    properties: WidgetProperties,
}

/// Implementation for drawing a `TextWidget`, with the `Widget` trait objects applied.
impl Widget for TextWidget {
    widget_default_impl!("TextWidget");

    /// This is the draw implementation of the `TextWidget`.  It uses the following properties:
    ///
    /// - PROPERTY_MAIN_COLOR: the `Color` of the body of the `Widget`
    /// - PROPERTY_BORDER_WIDTH: the width of the border to draw
    /// - PROPERTY_BORDER_COLOR: the `Color` of the border.
    /// - PROPERTY_FONT_COLOR: the color of the font
    /// - PROPERTY_FONT_NAME: full or relative path to the font file to use to render the text
    /// - PROPERTY_FONT_SIZE: the size in points of the font
    /// - PROPERTY_FONT_STYLE: the `FontStyle` to apply to the font
    /// - PROPERTY_TEXT_JUSTIFICATION: The `TEXT_JUSTIFY_*` constant to use to position the text inside the `Widget`
    /// - PROPERTY_TEXT: `String` containing the text to display
    fn draw(&mut self, c: &mut Canvas<Window>, t: &mut TextureCache) -> Option<&Texture> {
        // ONLY update the texture if the `BaseWidget` shows that it's been invalidated.
        if self.invalidated() {
            let bounds = self.properties.get_bounds();
            let (font_texture, width, height) = t.render_text(c, &mut self.properties, None);
            let text_justification = self.properties.get_value(PROPERTY_TEXT_JUSTIFICATION);
            let texture_y = 0;
            let widget_w = bounds.0;
            let texture_x: i32 = match text_justification {
                TEXT_JUSTIFY_LEFT => 0,
                TEXT_JUSTIFY_CENTER => (widget_w - width) as i32 / 2,
                TEXT_JUSTIFY_RIGHT => (widget_w - width) as i32,
                _ => 0,
            };

            self.texture_store
                .create_or_resize_texture(c, bounds.0, bounds.1);

            let cloned_properties = self.properties.clone();

            c.with_texture_canvas(self.texture_store.get_mut_ref(), |texture| {
                draw_base(texture, &cloned_properties, None);

                texture
                    .copy(
                        &font_texture,
                        None,
                        Rect::new(texture_x, texture_y, width, height),
                    )
                    .unwrap();
            })
            .unwrap();
        }

        self.texture_store.get_optional_ref()
    }
}
