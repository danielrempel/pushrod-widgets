// Pushrod Widgets
// Toggle Button Widget
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use sdl2::render::{Canvas, Texture};
use sdl2::video::Window;

use pushrod::caches::TextureCache;
use pushrod::event::PushrodEvent;
use pushrod::event::PushrodEvent::{
    DrawFrame, MouseButton, WidgetMouseEntered, WidgetMouseExited, WidgetSelected,
};
use pushrod::primitives::draw_base;
use pushrod::properties::{
    WidgetProperties, PROPERTY_BORDER_WIDTH, PROPERTY_ID, PROPERTY_TEXT_JUSTIFICATION,
    PROPERTY_TOGGLED, TEXT_JUSTIFY_CENTER, TEXT_JUSTIFY_LEFT, TEXT_JUSTIFY_RIGHT,
};
use pushrod::texture_store::TextureStore;
use pushrod::widget::Widget;
use pushrod::widget_default_impl;

use sdl2::pixels::Color;
use sdl2::rect::Rect;

/// Internal flag indicating if the button is currently accepting mouse button focus.
const PROPERTY_BUTTON_ACTIVE: u32 = 20000;

/// Internal flag indicating if the mouse is in the bounds of the `Widget`.
const PROPERTY_BUTTON_IN_BOUNDS: u32 = 20001;

/// Internal flag indicating whether or not this `Widget` originated the `MouseButton` state of `true`.
const PROPERTY_BUTTON_ORIGINATED: u32 = 20002;

/// Internal flag indicating whether or not this `Widget` has the mouse currently hovered over it, and
/// the button state is highlighted.
const PROPERTY_BUTTON_HOVERED: u32 = 20003;

/// Base Widget.
#[derive(Default)]
pub struct ToggleButtonWidget {
    texture_store: TextureStore,
    properties: WidgetProperties,
}

/// Auxiliary functions for the `ButtonWidget`.
impl ToggleButtonWidget {
    /// Sets the state to hovered, meaning the mouse has entered the bounds of the `Widget`.
    fn set_hovered(&mut self) {
        self.properties.set_bool(PROPERTY_BUTTON_HOVERED);
        self.invalidate();
    }

    /// Sets the state to unhovered, meaning the mouse has left the bounds of the `Widget`.
    fn set_unhovered(&mut self) {
        self.properties.delete(PROPERTY_BUTTON_HOVERED);
        self.invalidate();
    }

    /// Swaps the toggled state.
    fn toggle_selected(&mut self) {
        if self.properties.get_bool(PROPERTY_TOGGLED) {
            self.properties.delete(PROPERTY_TOGGLED);
        } else {
            self.properties.set_bool(PROPERTY_TOGGLED);
        }
    }

    /// Shortcut function to determine if the button is currently in a selected state or not.
    fn is_selected(&self) -> bool {
        self.properties.get_bool(PROPERTY_TOGGLED)
    }
}

/// Implementation for drawing a `BaseWidget`, with the `Widget` trait objects applied.
impl Widget for ToggleButtonWidget {
    widget_default_impl!("ToggleButtonWidget");

    /// This is a `ButtonWidget` that is used as a standard blank `Widget`.
    ///
    /// - PROPERTY_MAIN_COLOR: the `Color` of the body of the `Widget`
    /// - PROPERTY_BORDER_WIDTH: the width of the border to draw
    /// - PROPERTY_BORDER_COLOR: the `Color` of the border
    /// - PROPERTY_FONT_COLOR: the color of the font
    /// - PROPERTY_FONT_NAME: full or relative path to the font file to use to render the text
    /// - PROPERTY_FONT_SIZE: the size in points of the font
    /// - PROPERTY_FONT_STYLE: the `FontStyle` to apply to the font
    /// - PROPERTY_TEXT_JUSTIFICATION: The `TEXT_JUSTIFY_*` constant to use to position the text inside the `Widget`
    /// - PROPERTY_TEXT: `String` containing the text to display
    /// - PROPERTY_TOGGLED: boolean indicating whether or not the button is in a selected state
    ///
    /// While the button is selected, the color of the background will be `Color::BLACK`, and the
    /// text will turn `Color::WHITE`.  If the button is _not_ selected, it will revert back to the
    /// colors that you set for the text color and background colors of the button.
    fn draw(&mut self, c: &mut Canvas<Window>, t: &mut TextureCache) -> Option<&Texture> {
        // ONLY update the texture if the `BaseWidget` shows that it's been invalidated.
        if self.invalidated() {
            let bounds = self.properties.get_bounds();
            let text_color = if self.properties.get_bool(PROPERTY_BUTTON_HOVERED) {
                if self.is_selected() {
                    Some(Color::BLACK)
                } else {
                    Some(Color::WHITE)
                }
            } else if self.is_selected() {
                Some(Color::WHITE)
            } else {
                None
            };
            let (font_texture, width, height) = t.render_text(c, &mut self.properties, text_color);
            let text_justification = self.properties.get_value(PROPERTY_TEXT_JUSTIFICATION);
            let border_width = self.properties.get_value(PROPERTY_BORDER_WIDTH);
            let widget_w = bounds.0;
            let texture_x: i32 = match text_justification {
                TEXT_JUSTIFY_LEFT => 0,
                TEXT_JUSTIFY_CENTER => (widget_w - width) as i32 / 2,
                TEXT_JUSTIFY_RIGHT => (widget_w - width) as i32,
                _ => 0,
            };

            self.texture_store
                .create_or_resize_texture(c, bounds.0, bounds.1);

            let cloned_properties = self.properties.clone();
            let back_color = if self.properties.get_bool(PROPERTY_BUTTON_HOVERED) {
                if self.is_selected() {
                    Some(Color::WHITE)
                } else {
                    Some(Color::BLACK)
                }
            } else if self.is_selected() {
                Some(Color::BLACK)
            } else {
                None
            };

            c.with_texture_canvas(self.texture_store.get_mut_ref(), |texture| {
                draw_base(texture, &cloned_properties, back_color);

                texture
                    .copy(
                        &font_texture,
                        None,
                        Rect::new(
                            texture_x + border_width,
                            ((bounds.1 / 2) - (height / 2) - (border_width / 2) as u32) as i32,
                            width,
                            height,
                        ),
                    )
                    .unwrap();
            })
            .unwrap();
        }

        self.texture_store.get_optional_ref()
    }

    /// Overrides the `handle_event` function, which handles the mouse button, widget entering and
    /// exiting events.
    fn handle_event(&mut self, event: PushrodEvent) -> Option<PushrodEvent> {
        match event {
            WidgetMouseEntered { .. } => {
                if self.properties.get_bool(PROPERTY_BUTTON_ACTIVE) {
                    self.set_hovered();
                }

                self.properties.set_bool(PROPERTY_BUTTON_IN_BOUNDS);
            }
            WidgetMouseExited { .. } => {
                if self.properties.get_bool(PROPERTY_BUTTON_ACTIVE) {
                    self.set_unhovered();
                }

                self.properties.delete(PROPERTY_BUTTON_IN_BOUNDS);
            }
            MouseButton {
                widget_id,
                button,
                state,
            } => {
                let current_widget_id = self.properties.get_value(PROPERTY_ID);

                if button == 1 {
                    if state {
                        self.set_hovered();
                        self.properties.set_bool(PROPERTY_BUTTON_ACTIVE);
                        self.properties.set_bool(PROPERTY_BUTTON_ORIGINATED);
                    } else {
                        let had_bounds = self.properties.get_bool(PROPERTY_BUTTON_ACTIVE);

                        self.set_unhovered();
                        self.properties.delete(PROPERTY_BUTTON_ACTIVE);

                        if self.properties.get_bool(PROPERTY_BUTTON_IN_BOUNDS)
                            && had_bounds
                            && self.properties.get_bool(PROPERTY_BUTTON_ORIGINATED)
                            && current_widget_id as u32 == widget_id
                        {
                            self.properties.delete(PROPERTY_BUTTON_ORIGINATED);

                            self.toggle_selected();

                            return Some(WidgetSelected {
                                widget_id: current_widget_id as u32,
                                state: self.is_selected(),
                            });
                        }

                        self.properties.delete(PROPERTY_BUTTON_ORIGINATED);
                    }
                }
            }
            DrawFrame { .. } => {}
            _ => eprintln!("ButtonWidget Event: {:?}", event),
        }

        None
    }
}
